qual.pdf: $(wildcard *.tex)
	latexmk qual.tex --shell-escape -pdf -dvi- -ps-

clean:
	latexmk -c
	latexmk -CA
	rm qual.bbl
