\section{Database Papers}

\subsection{Incremental and Accuracy-Aware Personalized PageRank through Scheduled
Approximation}

By Zhu et al.\ in VLDB 2013. First set of questions:

\begin{enumerate}
    \item What does accuracy-awareness mean? Why is it important?

    FastPPV is accuracy-aware in reference to the $L_1$ error of its PPV vector
    approximations. Its computation is broken down into discrete steps, where
    each step contributes to the final PPV score. This is important so that we
    can create an iteration cutoff given a specific desired accuracy---this is a
    tradeoff between speed and accuracy which FastPPV allows the user to
    control.

    \item The accuracy, in terms of $L_1$ error, is given in the paper. Give the
    derivation of this error. Identify the intuition behind the equation.

    Since the PPVs are incrementally processed, we know the current $L_1$ error
    even without knowing the final answer. The error after iteration $k$ is
    %
    $$\rho^{(k)} = ||r_q - \hat{r}_q^{(k)}||_1 = \sum_{p\in V}|r_q -
    \hat{r}_q^{(k)}(p)|$$
    %
    Since $r_q(p)$ is a probability distribution over $V$, $\sum_{p\in
    V}r_q(p)=1$. Also, from theorem 1 in the paper, $r_q(p)\geq
    \hat{r}_q^{(k)}(p)$. Thus we can rearrange the terms as
    %
    $$\rho^{(k)} = \sum_{p\in V}r_q(p) - \sum_{p\in V}\hat{r}_q^{(k)}(p)
    = 1 - \sum_{p\in V}\hat{r}_q^{(k)}(p).$$
    %
    Thus we can calculate the error after each iteration without knowing the
    final exact PPV\@.

    \item The technique requires selecting a set of hubs, for preprocessing to
    build precomputed PPV ``segment'' values. The paper suggests a scheme
    considering the factors of decaying power and overall popularity of a node
    to select good hubs. We ask you to disagree with this scheme. Give two
    different factors of considerations for hub selection.

    \textbf{First}, we could use \textbf{betweenness centrality} instead of
    PageRank score to select the hubs. Betweenness centrality captures how often
    a node is on the shortest path between any other two nodes; this therefore
    places hubs in popular cross-areas between other node pairs, which enhances
    the visibility of the hubs more than PageRank would.

    \textbf{Second}, we would like to avoid hubs that are all clustered together
    in one area and neglect other areas. In real-world graphs there are giant
    connected components where most of the high-PageRank scoring nodes would
    lie. To avoid this undesirable clustering effect, we can select hubs such
    that they evenly cover the network. One way to achieve this would be to
    \textbf{run a clustering algorithm like $K$-means} for a few iterations,
    where $K=|H|$. Then, \textbf{assign a popular node in each cluster as a
    hub}; this ensures an even distribution of hubs that can contribute to many
    other nodes' PPVs.

\end{enumerate}

Second set of questions:

\begin{enumerate}
    \item What is the key insight of this paper for speeding up computation of
    Personalized PageRank?

    The key insight is that the PPV calculation can be broken down into discrete
    steps. Furthermore, they prove that step $i$ contributes exponentially more
    than step $i+1$; this allows the computation to be halted after $k$ steps,
    where $k$ is set to achieve a desired accuracy (since FastPPV's formulation
    allows it to be ``accuracy-aware'') or computation cost.

    \item How is ``Personalized'' PageRank different from the generic PageRank?
    Identify their similarities and differences. Give each one an example
    application scenario.

    In the original PageRank formulation, a random surfer moves to a neighboring
    node randomly with probability $1-\alpha$. With probability $\alpha$, the
    surfer transports to any other node on the graph. This process is repeated
    until the walk converges to a steady-state. An example application of this
    is to enhance search results with another measure that is related to
    centrality, in order to boost relevant pages or filter out spam pages (which
    have a low PageRank score).

    In Personalized PageRank, we can specify a query node $q$; this query node
    is where the $\alpha$ jumping probability takes the surfer. This creates
    a probability distribution around the neighborhood of $q$ as opposed to the
    entire graph (making the PageRank scores ``personalized'' for $q$). An
    example application of PPR is a friends recommendation system, where PPR
    runs on a social network.

    \item PageRank is one type of graph ranking metric for measuring connection
    between nodes on a graph. There are other metrics proposed in the
    literature, such as SimRank or Hitting Time. You may think of more examples.
    Do you think the general insight in this paper is applicable to other
    different metrics? Why?

    Yes, I believe that this metric may be applied to other measures that are
    based on random walks. In FastPPV, $r_q(P)$ is defined as a decomposable
    weighted sum of tours:
    %
    $$r_q(p) = \sum_{t\in q\rightarrow p} R(t)$$
    %
    where
    %
    $$R(t) =
    (1-\alpha)^{\mathcal{L}(t)}\cdot\alpha\cdot\prod_{t=0}^{\mathcal{L}(t)-1}
    \frac{1}{|\text{Out}(v_i)|}$$

    In hitting time, for example, the weighted sum is $p(t)\cdot\mathcal{L}(t)$
    which can be decomposed in the same way as PPV\@.

\end{enumerate}

\subsection{A Comparison of Approaches to Large Scale Data Analysis}

By Pavlo et al.\ in SIGMOD 2009. Summary: although the process to load data into
and tune the execution of parallel DBMSs took much longer than the MR system,
the observed performance of the DBMSs was strikingly better. It was Hadoop
against two DBMSs: Vertica and DBMS-X (a proprietary DBMS). They used a cluster
of 100 computers, with a shared-nothing architecture (partition- based
parallelism). In MR, there are $M$ partitions of input mapped to $R$ reducers
for a total of $M\times R$ files. The files need to be transferred to the Reduce
processes over the network. In contrast, the DBMS systems keep as much in memory
as possible.

A common theme is that the nature of the MR model is well-suited for dev
environments with a small number of programmers and a limited application
domain. The lack of constraints may not be appropriate for longer-term and
larger-sized projects.\\

\begin{tabular}{|p{3cm}|p{6cm}|p{6cm}|}
    \hline
    \textbf{Architectural element} & \textbf{MapReduce} & \textbf{Parallel DBMS}
    \\
    \hline Schema support & programmers always need to design and communicate
    the schema & schema must be defined separately from application (and has IC,
    etc) \\
    \hline
    Indexing & programmer needs to create indexes if desired & using proper
    index always reduces the search scope \\
    \hline
    Programming model & procedural/imperative, but also gaining support for
    higher-level with Pig, Hive, etc & relational/declarative, simpler \\
    \hline
    Data distribution & programmer decides & send computation to data \\
    \hline
    Execution strategy & pull: file transfer to nodes & push: move computation
    to data \\
    \hline
    Flexibility & Very general & Rigid, but has UDFs and application frameworks
    like rails that allow object-relational mappings \\
    \hline
    Fault tolerance & sophisticated failure model; easier since many
    intermediate files & since most data in memory, often need to completely
    restart task if there is a failure \\
    \hline
\end{tabular}\\

After these comparisons, some benchmarks were run:

\begin{enumerate}
    \item \textbf{Grep}, from the original MR paper.
    \begin{enumerate}
        \item 535MB per node (varying total number of nodes). MR much worse,
            though all scaled mostly linearly.
        \item 1TB evenly distributed across all nodes (varying total number of
            nodes). MR worse, but not by as much.
    \end{enumerate}
    For loading, the DBMSs were much slower since they had to index the data.
    Output in MR was also slow since the results had to be combined from all
    clusters (whereas the DBMS results were only in one file).
    \item \textbf{Filtering}: find PageRank URLs that are above a certain
        threshold. DBMSs destroy MR since they have an index on the PageRank
        field making the query trivial.
    \item \textbf{Aggregation}: calculate total revenue per source IP\@. Here,
        MR uses both M and R stages. Again, MR loses big time. Only interesting
        part is that since Vertica is column-based, it can save time by not
        reading the entire data for each row when it only needs IP and ad
        revenue data.
    \item \textbf{Join}: find source IP that generated the most ad revenue
        within date range, then calculate the average PageRank of all the pages
        visited in that interval. Important point is that two different datasets
        need to be used. MR program for this was \emph{way} more complicated
        than the DBMSs (which had about 10 lines of SQL). DBMSs took advantage
        of clustered indexes whereas the MR nodes had to do complete scans of
        20GB/data per node; hadoop limited by CPU overhead in file parsing. DBMS
        query optimizer allowed them to do local joins without any network
        communication before aggregating the final result.
    \item \textbf{UDF aggregation}: part of PageRank calculation: search for
        outgoing links given HTML text. Simple to write the parsing code in
        Java, but it was hard to get the UDFs for the DBMSs. In this experiment,
        DBMS-X was actually the worst, with MR just behind, with Vertica in
        front.
\end{enumerate}

\begin{tabular}{|p{3cm}|p{6cm}|p{6cm}|}
    \hline
    \textbf{Element} & \textbf{MapReduce} & \textbf{Parallel DBMS} \\
    \hline
    Installation and configuration & easy install; config for each task & hard
    install; config only once \\
    \hline
    Task startup & whenever job submitted (slow) & at OS boot \\
    \hline
    Compression & bad: slower and more user effort & good; less space and faster
    \\
    \hline
    Loading & if only loaded once, MR is better since it doesn't index &
    otherwise, worth it to process during load \\
    \hline
    Execution strategies & lots of message passing and control messages to sync
    processing & execution plan pushed to each node by query planner \\
    \hline
    Ease of use & easier to get started, but maintenance becomes a pain; reusing
    code hard; lots of deprecated functions & SQL more portable \\
    \hline
\end{tabular}

\subsection{GraphLab: A New Framework for Parallel Machine Learning}

By Low et al.\ in UAI 2010. Parallel abstractions like MR are insufficiently
expressive while low-level tools have experts repeatedly solving the same
problems. GraphLab targets common patterns in ML and compactly expresses
asynchronous iterative algorithms while ensuring data consistency and high
parallel performance. This is tested by designing and implementing parallel
versions of:

\begin{enumerate}
    \item Belief propagation (PB) on Markov random fields (MRF)
    \item Gibbs sampling
    \item Co-EM
    \item Lasso
    \item Compressed sensing
\end{enumerate}

Observation: most ML algorithms are sequential and therefore can't benefit as
much from the MR paradigm; there have been MR ML algorithms, but they often make
overly-simplifying assumptions or produce inefficient parallel algorithms that
require a large number of processors. Basically, MR fails when there are
computational dependencies in the data and it is not embarrassingly parallel.

GraphLab assumes everything fits in memory and has no fault tolerance which
reduces overhead in comparison to MR\@.

Data model: two parts: data graph and shared data table. The first encodes the
problem-specific sparse computational structure and modifiable program state.
Arbitrary data may be associated with each node. The shared data table is the
globally-shared state, such as hyperparameters or global convergence.

The \textbf{Jacobi schedule} is for things like gradient descent and is more
parallel. The \textbf{Gauss-Seidel schedule} is more sequential. Thus, in Jacobi
iteration updating occurs when all nodes have been recalculated, while in
Gauss-Seidel, each node is updated directly when its new value is found. Unlike
the Jacobi method, only one storage vector is required as elements can be
overwritten as they are computed, which can be advantageous for very large
problems. However, unlike the Jacobi method, the computations for each element
in Gauss-Seidel cannot be done in parallel. Furthermore, the values at each
iteration are dependent on the order of the original equations.

Jacobi schedule is implemented as a \textbf{synchronous scheduler} (ensures all
$v$ updated at once) and Gauss-Seidel is implemented as a \textbf{round-robin
scheduler} (updates vertices sequentially using the most recently available
data).

The GraphLab abstraction provides three data consistency models which enable the
user to balance performance and data consistency. Essentially, the consistency
model determines to what extent overlapping scopes can be executed
simultaneously. This is accomplished with \textbf{exclusion sets}, where the
local update functions never share overlapping exclusion sets.

\begin{itemize}
    \item The \textbf{full consistency model} ensures that during the execution
        of $f(v)$ no other function will read or modify data within $S_v$.
        Therefore, parallel execution may only occur on vertices that do not
        share a common neighbor (Shooting $L_1$ since bipartite).
    \item The slightly weaker \textbf{edge consistency model} ensures that
        during the execution of $f(v)$ no other function will read or modify any
        of the data on $v$ or any of the edges adjacent to $v$. Under the edge
        consistency model, parallel execution may only occur on non-adjacent
        vertices. (LoopyBP)
    \item The weakest \textbf{vertex consistency model} only ensures that during
        the execution of $f(v)$ no other function will be applied to $v$. The
        vertex consistency model is therefore prone to race conditions and
        should only be used when reads and writes to adjacent data can be done
        safely (In particular repeated reads may return different results).
        However, by permitting update functions to be applied simultaneously to
        neighboring vertices, the vertex consistency model permits maximum
        parallelism. (Gibbs with G-S)
\end{itemize}

System summary:

\begin{enumerate}
    \item A data graph which represents the data and computational dependencies
    \item Update functions which describe local computation
    \item A Sync mechanism for aggregating global state
    \item A data consistency model (i.e., Fully Consistent, Edge Consistent or
        Vertex Consistent), which determines the extent to which computation can
        overlap
    \item Scheduling primitives which express the order of computation and may
        depend dynamically on the data
\end{enumerate}

They don't actually compare to other implementations cited in the related work;
the only graphs are of their speedup versus number of cores.

\textbf{Potential Questions}

\begin{enumerate}
    \item What other algorithms can be applied to this framework?
    \begin{itemize}
        \item Averaged SGD, where we learn weight vectors in parallel and then
            average them. For normal SGD, $L_1$ calculation is similar
        \item PageRank calculation, where each node is actually a page and the
            edges are links. Use edge consistency
    \end{itemize}
    \item hmm
\end{enumerate}

\subsection{CrowdScreen: Algorithms for Filtering Data with Humans}

By Parameswaran in SIGMOD 2012. Given a large set of data items, this considers
the problem of filtering them based on a set of properties that can be verified
by humans (as in, Mechanical Turk workers). We want to optimize the number of
questions and expected error. The main contributions are:

\begin{itemize}
    \item Grid-based visualization
    \item Deterministic strategies
    \item Probabilistic strategies
    \item Extensions
\end{itemize}

Most of the paper deals with the single (Y/N) filter problem, though it's
possible to generalize to multiple filters or $n$-ary filters. The grid
visualization denotes a point $(x,y)$ as receiving $x$ NO and $y$ YES responses.
Each point is colored blue (output pass), red (output fail), or green (continue
asking questions). Pass and fail outputs terminate the question-asking. In the
probabilistic model, each point on the grid is a distribution over the possible
three values. $E(x,y)$ is the probability of error given that the strategy
terminated at $(x,y)$. $C(x,y)$ is the number of questions asked to get to that
point, which is simply $x+y$. $E$ and $C$ are the expected error and cost. With
this framework, there are five problems that may be solved with (optionally)
error threshold $\tau$ and budget threshold per item $m$:

\begin{enumerate}
    \item (Core) Find a strategy that minimizes $C$ under the constraint $E
        <\tau$ and $\forall(x, y), C(x, y) < m$.
    \item (Core: per-item error) Find a strategy that minimizes $C$ under the
        constraint $\forall(x, y), E(x, y) < \tau$ and $C(x, y) < m$.

        Solution: we can show that we simply need to compute $R(x, y)$ for every
        point in $(0,0)\rightarrow (m,m)$, bottom up, and for every point where
        we find that $min(R(x, y), 1-R(x, y)) < \tau$, we make the point a
        terminating point, returning Pass if $R(x, y) \leq 0.5$ and Fail
        otherwise. This is $O(m^2)$.
    \item (Maximum cost) Find a strategy that minimizes the maximum value of
        $C(x, y)$ (over all points), under the constraint $E < \tau$.

        Solution: This problem formulation
        is identical to MAP estimation. In
        this case we simply ask all $m$ questions (since there is no gain to
        asking fewer than $m$ questions). We can estimate the $p_0$ and $p_1$ at
        all points along $x + y = m$ for this triangular strategy. If $p_0 > p_1$,
        we return Fail at that point and Pass otherwise. We can then estimate
        the error $E$ over all the terminating points. This is $O(m^2)$ for
        problem 4. For 3, perform repeated
        doubling of the maximum cost $m$, until we find a triangular shape
        for which $E < \tau$, followed by binary search between the two
        thresholds $m$ and $\frac{m}{2}$. This is $O(m^2\log m)$.
    \item (Error) Find a strategy that minimizes $E$ under the constraint
        $\forall(x, y), C(x, y) < m$.

        Solution: see previous.
    \item (Error - II) Given a cost threshold $\alpha$, find a strategy that
        minimizes $E$ under the constraints $\forall(x, y), C(x, y) < m$ and $C
        < \alpha$.

        Solution: use the same linear programming formulation, except that we
        constrain $C$ and minimize $E$. This is again $O(m^4)$.
\end{enumerate}

Deterministic and exact strategies:

\begin{itemize}
    \item \textbf{naive3}: solution for problem 1 in $O(m^23^m)$. Brute force
        from $(0,0)\rightarrow (m,m)$. $3^m$ since there are three possible
        assignments: pass, fail, continue.
    \item \textbf{naive2}: using the paths principle, we can see that the way we
        get to a point doesn't matter since we can only go to the right and up;
        thus we only consider whether it terminates or continues, which is $2^m$
        strategies. If it terminates, we can use the theorem to see whether it
        was pass or fail by seeing if $R(x,y)=\frac{p_0(x,y)}{p_0(x,y) +
        p_1(x,y)}$ is greater or less than a half. This gives $O(m^22^m)$ time.
\end{itemize}

Deterministic and heuristic strategies use \textbf{shapes} (series of connected
lines with only one decision point and terminating points only along the
boundary. They conjecture that the best strategy is always a shape but don't
prove it (also meaning each shape corresponds to exactly one strategy).
Additionally, the say one subset of shapes (\emph{i.e.} ladder shapes) always
contain the best strategy. Ladder shapes are shapes that only go to the right
and up (make ladders/stairs on the top and bottom borders). Any shape may be
converted into a ladder shape yielding lesser cost with the same error.

In the probabilistic models, the cost at a given point is the probability that
the strategy terminates at that point times the total number of questions asked
to get there. This contains some nonlinear constraints (those that are products
of two variables), but the paths principle from before can be used to transform
it into a linear program by using two new variables $tPath(x,y)$ (number of paths in
strategy that terminate there) and $cPath(x,y)$ (fractional count of paths from
$(0,0)\rightarrow (x,y)$ that continue). Thus, $tPath(x,y) + cPath(x,y)$ is the
total number of paths reaching $(x,y)$.

\begin{itemize}
    \item \textbf{linear}: is the linear program that can solve the
        probabilistic formulation in $O(m^4)$ time since there are $m^2$
        variables in $\forall (x,y);x+y\leq m:\ldots$ and linear programs'
        running time are quadratic in number of constraints and variables.
    \item \textbf{ladder}: (shape heuristic), described above.
    \item \textbf{growth}: (heuristic) grow the space from $(0,0)$ until the
        constraints are met.
    \item \textbf{shrink}: (heuristic) shrink a strategy until the cost cannot
        be decreased any longer starting with a triangular strategy.
    \item \textbf{rect}: (rekt) (heuristic), tried all rectangular strategies
        that fit in $(m,m)$.
\end{itemize}

Claimed experimental results comparing heuristic deterministic algorithms.

\begin{itemize}
    \item Results on varying $m$. Ladder has large cost savings and its cost
        decreases as $m$ increases.
    \item Results on varying $s=P(V=1)$. Growth sometimes gets stuck in local
        minima; if not, shrink and growth outperform rect.
    \item Results on varying $e_1=P(NO|V=1)$. Cost increases superlinearly all
        around (not surprising).
\end{itemize}

Claimed experimental results comparing ladder, point, and linear. Results on
varying $m$. Linear performs even better than ladder, and both perform better
than point, which sometimes returns infeasible solutions.

On average, linear and ladder outperform other algorithms on varying $m$. For
small $m$, shrink does well.

\textbf{Multiple filters}. The multiple filters problem can be formulated as a
general version of the linear program and results in a cost of
$\Omega(m^{4k})$, where $k$ is the number of filters. This is because a
decision needs to be provided for each point in the $2k$-dimensional space.

Other questions:

\begin{itemize}
    \item Why is it problematic to assume that humans are identical,
        independent, noisy oracles with fixed error rates?
    \item Multiple answers versus multiple filters
    \item This paper assumes that error rates are known; why is that
        problematic?

        How do you test? Testing costs money
        If error rates are imprecise, optimization is useless.
    \item How would you go about gauging human error rates on a batch of
        filtering tasks that you've never seen before?

        You could have the requester create gold standard questions, but hard:
        people learn, high cost, doesn't capture all issues You could try to use
        majority rule but what about difficulty, what about expertise?

    \item Let's say someone did all you asked: they created a gold standard,
        tested humans on it, generated an optimized strategy, but that is still
        too costly. How can you help them reduce costs?

        Improve instructions and interfaces. Training and elimination. Only
        allow good workers to work on tasks. Use machine learning.
    \item What other scenarios (beyond crowdsourcing) can these algorithms be
        useful?

        Anywhere you have noisy oracles! Lab experiments, automobile testing,
        medical diagnosis.

    \item Multiple answers. Why is this generalization problematic? How would
        you fix it?

        You need $O(n^k)$ data points. Use generic error model, bucketize.
        Priors.

    \item What are the different ways crowd algorithms like this can be used in
        conjunction with machine learning?

        Input/training. Active learning. ML feeds crowds.
\end{itemize}
