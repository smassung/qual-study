\section{Databases}

\textbf{Query languages} allow manipulation and retrieval of data
from a database. Queries and operations are posed with respect to a data model;
the relational data model supports simple, powerful query languages that have
a strong formal foundation based on logic and allows for automatic optimization.
SQL is an example of such a query language.

Query languages are not programming languages; they are not expected to be
Turing complete. They simply need to support easy, efficient access to large
data sets. They are not intended for complex calculations.

\textbf{Relational algebra} is composed of five basic operations: selection
(select subset of rows), projection (delete unwanted columns), cross-product
(combine two relations), set-difference (tuples in lhs but not rhs), and union
(tuples in lhs and rhs). All operations also return a relation, so operations
may be composed. Other additional operators are intersection, join, division,
and renaming. Relational algebra is procedural (describes an algorithm) whereas
SQL is non-procedural (describes what you want).

\textbf{Integrity constraints} are conditions that must be true for any instance
of the database; they are specified when the DB is defined and checked when
relations are modified. A legal instance of a relation is one that satisfies all
integrity constraints. If a DBMS checks the constraints, the stored data is more
faithful to its real-world meaning while also avoiding data entry errors.

A \textbf{NoSQL} (non-relational; ``not-only SQL'') databases provides a
mechanism for storage and retrieval of data that is modeled in means other than
the tabular relations used in relational databases. Motivations for this
approach include simplicity of design; simpler ``horizontal'' scaling to
clusters of machines, which is a problem for relational databases; and finer
control over availability. The data structures used by NoSQL databases (like
key-value, graph, or document) differ slightly from those used by default in
relational databases, making some operations faster in NoSQL and others faster
in relational databases.

Some examples of NoSQL databases are Redis (data structure storage), MongoDB
(document storage, JSON-like), Cassandra (scalability- and availability-focused
key-value storage). Neo4j is a graph database. Aside from the indexing
flexibility, a main motivation for using NoSQL databases is their much better
ability to scale to large amounts of ``big data'' than traditional RDBMS\@.

Random vocab: ACID (Atomicity, Consistency, Isolation, Durability) is a set of
properties that guarantee that database transactions are processed reliably.

DBMS must provide efficient access to data. Declarative queries are translated
into imperative query plans. A declarative query is what the user types when
interacting with the DBMS; they imagine the logical data model. The DBMS
converts the declarative queries into imperative plans that interact with the
physical structure of the database. Relational optimizers aim to find the best
imperative plan (\emph{i.e.}, with lowest execution times). In practice, they
avoid the \emph{worst} execution times.

Concurrency control is another important aspect of a DBMS.

\subsection{Parallel and Distributed Databases}

A \textbf{parallel database} is a database that can do multiple tasks in
parallel allowing the database to make use of multiple CPU cores and multiple
disks that are standard for modern database servers. However, all CPU cores can
directly address all disks in a parallel database. Machines are physically close
to each other (\emph{e.g.} the same server room). Machines connect with
dedicated high-speed LANs and switches, so the communication cost is assumed to
be small. Parallel database design can be a shared-memory, shared-disk, or
shared-nothing architecture. These have existed since the late 1980s with Gamma
and Grace as examples.

A \textbf{distributed database} is a database where data is distributed across
multiple hosts. However, the CPUs of a given host can only directly address a
subset of the disks---those disks that are on that host. This means that
machines can be far from each other (\emph{e.g.} in different continents). They
can be connected using a public-purpose network like the internet, which makes
communication cost and problems not able to be ignored. Usually, due to these
constraints, this is a shared-nothing architecture. Distributed databases are
usually implemented as \textbf{Map-Reduce}.

\begin{itemize}
    \item Schema support:
    \begin{itemize}
        \item PD: (Y) Relational paradigm: rigid structure of rows and columns
        \item DD: (N) Flexible structure, but need to write parsers and
            challenging to share results
    \end{itemize}
    \item Indexing
    \begin{itemize}
        \item PD: (Y) B-trees to speed up access
        \item DD: (N) No built-in indexes --- programmers must code indexes
    \end{itemize}
    \item Programming model
    \begin{itemize}
        \item PD: Declarative, high-level language (SQL)
        \item DD: Imperative, write programs (Java)
    \end{itemize}
    \item Data distribution and optimizations
    \begin{itemize}
        \item PD: (Y) Use knowledge of data distribution to automatically
            optimize queries
        \item DD: (N) Programmer must optimize the access
    \end{itemize}
    \item Execution strategy and fault tolerance:
    \begin{itemize}
        \item PD: (N) Pipeline operators (push), failures dealt with at the
            transaction level
        \item DD: (Y) Write intermediate files (pull), provide fault tolerance
    \end{itemize}
\end{itemize}

In experimental evaluation by Pavlo et al. (2009) and Stonebraker et al. (2010),
MapReduce startup costs were shown to dominate the execution times when compared
to Parallel DBMS\@. Argument for MapReduce: designed for complex tasks that
manipulate diverse data such as extracting links from Web pages and aggregating
them by target document; generating inverted index files to support efficient
search queries; processing all road segments in the world and rendering map
images. These data do not fit well in the relational paradigm! Remember: SQL is
not Turing-complete. HadoopDB: hybrid of DBMS and MapReduce technologies that
targets analytical workloads Twister: enhanced runtime that supports iterative
MapReduce computations efficiently.

\subsection{Data partitioning schemes}

Current high end relational database management systems provide for different
criteria to split the database. They take a partitioning key and assign a
partition based on certain criteria. Common criteria are:

\begin{itemize}
    \item \textbf{Range partitioning}: selects a partition by determining if the
        partitioning key is inside a certain range. An example could be a
        partition for all rows where the column zipcode has a value between
        70000 and 79999.
    \item \textbf{List partitioning}: A partition is assigned a list of values.
        If the partitioning key has one of these values, the partition is
        chosen. For example all rows where the column Country is either Iceland,
        Norway, Sweden, Finland or Denmark could build a partition for the
        Nordic countries.
    \item \textbf{Hash partitioning}: The value of a hash function determines
        membership in a partition. Assuming there are four partitions, the hash
        function could return a value from 0 to 3.
    \item \textbf{Composite partitioning}: allows for certain combinations of
        the above partitioning schemes, by for example first applying a range
        partitioning and then a hash partitioning. Consistent hashing could be
        considered a composite of hash and list partitioning where the hash
        reduces the key space to a size that can be listed.
\end{itemize}

The partitioning can be done by either building separate smaller databases (each
with its own tables, indices, and transaction logs), or by splitting selected
elements, for example just one table.

\textbf{Horizontal partitioning} (also see shard) involves putting different
rows into different tables. Perhaps customers with ZIP codes less than 50000 are
stored in CustomersEast, while customers with ZIP codes greater than or equal to
50000 are stored in CustomersWest. The two partition tables are then
CustomersEast and CustomersWest, while a view with a union might be created over
both of them to provide a complete view of all customers.

\textbf{Vertical partitioning} involves creating tables with fewer columns and
using additional tables to store the remaining columns. Normalization also
involves this splitting of columns across tables, but vertical partitioning goes
beyond that and partitions columns even when already normalized. Different
physical storage might be used to realize vertical partitioning as well; storing
infrequently used or very wide columns on a different device, for example, is a
method of vertical partitioning. Done explicitly or implicitly, this type of
partitioning is called ``row splitting'' (the row is split by its columns). A
common form of vertical partitioning is to split dynamic data (slow to find)
from static data (fast to find) in a table where the dynamic data is not used as
often as the static. Creating a view across the two newly created tables
restores the original table with a performance penalty, however performance will
increase when accessing the static data (\emph{e.g.} for statistical analysis).

\subsection{Indexing}

A B+ tree is a B tree where all the data lies in the leaf nodes. Each interior
node only contains keys. All leaves are liked together to allow easy range
queries.

Because B+ trees don't have data associated with interior nodes, more keys can
fit on a page of memory. Therefore, it will require fewer cache misses in order
to access data that is on a leaf node. The leaf nodes of B+ trees are linked, so
doing a full scan of all objects in a tree requires just one linear pass through
all the leaf nodes. A B tree, on the other hand, would require a traversal of
every level in the tree. This full-tree traversal will likely involve more cache
misses than the linear traversal of B+ leaves.

Because B trees contain data with each key, frequently accessed nodes can lie
closer to the root, and therefore can be accessed more quickly.

\subsection{Old Questions}

\begin{enumerate}
    \item What is the most important property of a query language? Explain why.

    It must map to relational algebra, which maps to the hardware supporting the
    queries.

    \emph{Alternate}: It must accurately (unambiguously) express an
    information need (e.g., an information retrieval query language must
    allow me to exactly specify what I expect to be true about the
    retrieved documents).

    \item How do you think parallel query processing in Parallel DBMS (such as
        Gamma) is different from Map-Reduce? Identify one major difference.

    Paper.

    \item Indexing helps query processing. Other than speeding it up (making it
        more efficient), give one more major advantage that indexing enables for
        query processing.

    Allows the translated relational algebra query to interact with the DB in a
    principled way, ensuring correctness of the result.

    \emph{Alternate}: Sorted indexes give you sorting for free for e.g.\
    \texttt{ORDER BY} queries (no need to do an explicit merge of the
    tuples). Unique indexes are useful for ensuring relational constraints
    (like \texttt{PRIMARY KEY}, \texttt{UNIQUE}).

    \item As built-in data types in a DBMS are often restrictive, it is
        desirable to support extensible types. Identify one significant
        complication for supporting type extensibility for query processing.
        Explain why.

        The implementation of new access paths for DBMSs, native integration of
        unstructured data types (like XML) or other core extensions usually
        results in major modifications of the DBMS kernel. Namely, the
        integration of a new index structure requires changes in the SQL
        compiler, query plan optimizer, access path layer, cache manager, lock
        manager, and the physical storage layer. The latter is also likely to
        affect logging and recovery facilities.

    \item In parallel databases, is the data partitioning scheme (to spread data
        across different computing nodes) important for query efficiency?
        Explain why.

        Yes.

    \item Some may say that relational algebra is a better query language than
        SQL\@. Give one argument to support this statement.

        This allows the programmer more control over how their query is
        executed as opposed to declarative SQL statements (relational
        algebra is procedural vs.\ the declarative SQL).

    \item Identify one major point of how NoSQL DBMS differs from Relational
        DBMS\@. Explain it with an example.

    They support a much richer set of indexed data types; for example, Neo4j is
    a DB that can efficiently store graph objects.

    \item What are the key requirements for an index structure for DBMS\@?
        Identify two points.

        \begin{itemize}
            \item Must Support efficient lookup
            \item Can be used to enforce key constraints
        \end{itemize}

    \item Is relational algebra a query language? Explain why.

    Yes: it provides a mechanism for specifying the data one wishes to
    obtain from a DBMS system. It's perhaps too low-level (in that it
    specifies exactly \emph{how} the DBMS is required to fetch the data),
    but it still adequately and unambiguously describes the information
    need.

\end{enumerate}
