\section{Data Mining}

\subsection{Data Warehousing}

A \textbf{data warehouse} is a subject-oriented (simple, concise), integrated
(multiple data sources), time-variant, and nonvolatile collection of data in
support of management's decision-making processes.

\textbf{OLAP} (online analytical processing) tools enable users to analyze
multidimensional data interactively from multiple perspectives. OLAP consists of
three basic analytical operations: consolidation (\textbf{roll-up}),
\textbf{drill-down}, and \textbf{slicing and dicing}. Consolidation involves the
aggregation of data that can be accumulated and computed in one or more
dimensions. For example, all sales offices are rolled up to the sales department
or sales division to anticipate sales trends. By contrast, the drill-down is a
technique that allows users to navigate through the details. For instance, users
can view the sales by individual products that make up a region's sales. Slicing
and dicing is a feature whereby users can take out (slice) a specific set of
data of the OLAP cube and view (dice) the slices from different viewpoints.
These viewpoints are sometimes called dimensions, such as looking at the same
sales by salesman or by date or by customer or by product or by region, etc.

At the core of any OLAP system is a \textbf{data cube}. This consists of numeric
facts called measures which are categorized by dimensions. The measures are
placed at the intersections of the hypercube, which is spanned by the dimensions
as a vector space. The usual interface to manipulate an OLAP cube is a matrix
interface, which performs projection operations along the dimensions, such as
aggregation or averaging.

Other data cube operations described in CS 412 are \textbf{pivot} (like the
aggregation mentioned above), \textbf{drill across} (using more than one table),
and \textbf{drill through} (using the backend SQL tables).

A \textbf{star schema} is the simplest style of a data mart schema. The star
schema consists of one or more fact tables referencing any number of dimension
tables. The star schema is an important special case of the snowflake schema,
and is more effective for handling simpler queries. The star schema gets its
name from the physical model's resemblance to a star shape with a fact table at
its center and the dimension tables surrounding it representing the star's
points. The main disadvantage of the star schema is that data integrity is not
enforced as well as it is in a highly normalized database. One-off inserts and
updates can result in data anomalies which normalized schemas are designed to
avoid.

\subsection{Pattern Mining}

\textbf{Frequent pattern mining}  finds relationships among the items in a
database. The problem can be stated as follows. ``Given a database $D$ with
transactions $T_1,\ldots,T_N$, determine all patterns $P$ that are present in at
least a fraction $s$ of the transactions. The fraction $s$ is referred to as the
minimum support. The problem was originally proposed in the context of market
basket data in order to find frequent groups of items that are bought together.
Thus, in this scenario, each attribute corresponds to an item in a superstore,
and the binary value represents whether or not it is present in the transaction.

In general, \textbf{association rules} can be considered a second-stage output,
which are derived from frequent patterns. Consider the sets of items $U$ and
$V$. The rule $U\implies V$ is considered an association rule at minimum support
$s$ and minimum confidence $c$, when the following two conditions hold true:

\begin{enumerate}
    \item The set $U\cup V$ is a frequent pattern
    \item The ratio of support of $U\cup V$ to that of $U$ is at least $c$
\end{enumerate}

Because the first step of finding frequent patterns is usually the
computationally more challenging one, most of the research in this area is
focussed on this as opposed to finding association rules. The level-wise
property of frequent pattern mining is algorithmically crucial because it
enables the design of a bottom-up approach to exploring the space of frequent
patterns. In other words, a $(k+1)$-pattern may not be frequent when any of its
subsets is not frequent. This is a crucial observation that is used by virtually
all the efficient frequent pattern mining algorithms.

Some common frequent pattern mining methods are:

\begin{itemize}
    \item \textbf{Apriori} also known as \emph{aperi} or BFS\@. Candidate
        itemsets are generated in increasing order of itemset size. These
        itemsets are then tested against the underlying transaction database and
        the frequent ones satisfying the minimum support constraint are retained
        for further exploration.
    \item \textbf{FP-growth} also known as DFS\@. The depth-first method has
        been shown to have a number of advantages in maximal pattern mining,
        because of the greater effectiveness of the pruning-based lookaheads in
        the depth-first strategy. It avoids the many repeated scans that Apriori
        requires.
\end{itemize}

\textbf{Max-miner} tries to identify the maximal frequent item sets without
enumerating their subsets, and perform ``jumps'' in the search space rather than
a purely bottom-up approach such as Apriori.

Closed frequent itemsets are defined as frequent patterns, no superset of which
have the same frequency as that itemset. By mining closed frequent itemsets, it
is possible to significantly reduce the number of patterns found, without losing
any information about the support level.

\textbf{Sequential pattern mining} is related to frequent pattern mining, but
here there is an order to the transactions. Some common methods are:

\begin{itemize}
    \item \textbf{GSP algorithm} (generalized sequential pattern). The most
        basic method which is essentially Apriori for sequences.
    \item \textbf{FreeSpan}: frequent pattern-projected sequential pattern
        mining. Improvement over GSP via projections.
    \item \textbf{PrefixSpan}, an improvement over the previous two methods. No
        candidate sequence needs to be generated by PrefixSpan. The projected
        databases keep shrinking and the major cost of PrefixSpan is the
        construction of projected databases.
\end{itemize}

\subsection{Clustering}

A cluster is a collection of data objects that are similar to one another within
the same cluster and are dissimilar to the objects in other clusters. The
process of grouping a set of physical or abstract objects into classes of
similar objects is called \textbf{clustering}. Some common methods are:

\begin{itemize}
    \item \textbf{AGNES}: basic hierarchical agglomerative clustering.
    \item \textbf{DIANA}: basic divisive clustering. Like $k$-means,
        $k$-medoids, and CLARANS.
    \item \textbf{BIRCH}: uses clustering feature (CF) trees to store DB info.
        Designed to minimize IO operations, and in fact operates in $O(n)$ time.
        It is designed for clustering a large amount of numeric data by
        integrating hierarchical clustering (at the initial microclustering
        stage) and other clustering methods such as iterative partitioning (at
        the later macroclustering stage). It overcomes scalability issues and
        undo issues.
    \item \textbf{Chameleon}:  hierarchical clustering based on $k$-nearest
        neighbors and dynamic modeling. Construct sparse graph; partition graph;
        merge partitions.
    \item \textbf{DBSCAN}: Density-Based Spatial Clustering of Applications with
        Noise) finds core objects, that is, objects that have dense
        neighborhoods. It connects core objects and their neighborhoods to form
        dense regions as clusters. Other density-based algorithms are OPTICS
        (frees user from choosing parameters) and DENCLUE (uses nonparametric
        kernel density estimation).
    \item \textbf{Grid-based methods}. These approaches use a multiresolution
        grid data structure. It quantizes the object space into a finite number
        of cells that form a grid structure on which all of the operations for
        clustering are performed. The main advantage of the approach is its fast
        processing time, which is typically independent of the number of data
        objects, yet dependent on only the number of cells in each dimension in
        the quantized space. Examples: STING and CLIQUE.
    \item With constraints, you can use an algorithm such as
        \textbf{COP-$k$-means}.
\end{itemize}

\subsection{Network Analysis}

\textbf{Homogeneous networks} have only one type of edge and node. An example is
a social network where all nodes are people and all edges link two people
together in a friendship. Some tasks and algorithms on homogeneous networks are:

\begin{itemize}
    \item Clustering. \textbf{SCAN} finds a cut of the graph, where each cluster
        is a set of vertices that are connected based on the transitive
        similarity in a structural context. An advantage of SCAN is that its
        time complexity is linear with respect to the number of edges. In very
        large and sparse graphs, the number of edges is in the same scale of the
        number of vertices. Therefore, SCAN is expected to have good scalability
        on clustering large graphs.
    \item Link prediction. In link prediction, it's often necessary to calculate
        the similarity between two nodes. One popular method is
        \textbf{SimRank}: a similarity measure based on random walk and on the
        structural context of a graph. A simpler method is \textbf{geodesic
        distance}, which is just the shortest path between two nodes. Others are
        \textbf{Katz} score, Jaccard coefficient, and common neighbors.
\end{itemize}

\textbf{Frequent subgraph mining} is another task on information networks. It is
the discovery of graph structures that occur a significant number of times
across a set of graphs. For example: the common occurrence of the hydroxide ion
in chemistry (where molecules are graphs), common biological pathways among
species, or recurring patterns of humans interaction during an epidemic.

Some FSM algorithms are:

\begin{itemize}
    \item \textbf{gSpan}. Complete frequent subgraph mining that improves
        performance over straightforward apriori extensions to graphs through
        DFS code representation and aggressive candidate pruning.
    \item \textbf{SUBDUE}. Approximates frequent subgraph mining by using graph
        compression as a metric for determining a ``frequently occurring''
        subgraph.
    \item \textbf{SLEUTH}. Complete frequent subgraph mining built specifically
        for trees.
\end{itemize}

Centrality measures capture the importance of a particular node in a network.

\begin{itemize}
    \item \textbf{Degree centrality}: number of links.
    \item \textbf{Closeness centrality}: average of shortest path from the node
        to every other node.
    \item \textbf{Betweenness centrality}: number of shortest paths from all
        vertices to all others that pass through it; indicates monitoring role
        for various pairs.
    \item \textbf{Eigenvector centrality}: also known as prestige. Calculated
        with power iteration usually, and considers more than only adjacent
        nodes (ripple effect).
    \item \textbf{PageRank}: normalized prestige; random surfing model.
    \item \textbf{HITS}: capture both co-citation and bibliographic coupling via
        authorities (highly-cited) and hubs (point to good authorities).
\end{itemize}

Real-world networks have common characteristics: a few connected components,
small diameter, high clustering, heavy-tailed degree distribution (power law).
There are a small but reliable number of high-degree vertices. Different random
graph models try to capture this phenomenon. Some are:

\begin{itemize}
    \item \textbf{Erdos-Remy}: few components, small diameter.
        No high clustering, or heavy-tailed degree distribution. To create:
        randomly add edges.
    \item \textbf{Watts-Strogatz} small world model: few components, small
        diameter, high clustering. No heavy-tailed distribution. It assumes
        constant and independent probability of two nodes connecting which means
        no local clustering. Degree dist follows Poisson dist, not power
        law.
    \item \textbf{Barabasi-Albert Scale-Free} model: few components, small
        diameter, heavy-tailed distribution. No high clustering. It fixes major
        limitations in W-S model (homogeneous degrees) and uses preferential
        attachment: edges from new a vertex are more likely to link to nodes
        with higher degrees.
\end{itemize}

A \textbf{heterogeneous information network} includes nodes of different types.
For example, a bibliographic information network can contain author, paper, and
venue nodes. A \textbf{metapath} is a particular pattern of edges that traverses
nodes of a certain type. In the bibliographic network example, a metapath could
be $A-P-V-P-A$, which represents the path from one author to a paper, to a venue
where the paper is published, to another paper from the same venue, to an author
from the paper. This is one metapath that links two authors together with the
meaning ``publish papers in the same venues''.

\begin{itemize}
    \item Clustering. \textbf{RankClus}: ranking and clustering integration;
        highly ranked authors publish in highly ranked conferences; highly
        ranked conferences attract highly-ranked authors; simply says
        \emph{mutually enhance}. \textbf{RankClass}: ranking and classification
        integration. \textbf{GNetMine} also does classification on hetnets;
        downside is that all objects are treated equally.
    \item Link prediction. An algorithm is \textbf{PathPredict}, which uses
        metapath similarity measures between nodes to predict coauthorships on
        DBLP data. Another version called \textbf{PathPredict\_when} takes time
        into account.
\end{itemize}

Some miscellaneous algorithms are \textbf{DISTINCT}: name disambiguation in
bibliographic network. \textbf{TruthFinder}: among conflicting results, find the
truth; example is book authors on various online retailers. Succeeded by
\textbf{Latent Truth Model} (LTM) which supports multiple true attribute values
and incorporates priors. Finally succeeded by \textbf{Gaussian Truth Model}
(GTM) which assigns real-values truth scores as opposed to categorical truth
like in LTM\@. \textbf{GraphCube} encodes OLAP as a graph via aggregated
networks.


\subsection{Algorithm Table}

\begin{tabular}{l|p{0.6\textwidth}}
    \textbf{Task} & \textbf{Algorithm} \\
    \hline
    Cubing (generally) & BUC, H-cubing, multi-way aggregation, star cubing,
    shell fragment cubing, sampling cube, ranking cube, \ldots\\
    Iceberg Cubing & BUC, Star Cubing\\
    High-dimensional Cubing & Shell Fragment Cubing\\
    Clustering & AGNES, DIANA, BIRCH (hierarchical, minimize IO), Chameleon
    (hierarchical $k$-NN), COP-$k$-means \\
    Clustering weird shapes & DBSCAN \\
    Clustering subgraphs (homo) & SCAN \\
    Frequent subgraph patterns (homo) & gSpan, SUBDUE, SLEUTH \\
    Classifying subgraphs (homo) & LEAP, gPLS (?) \\
    Graph centrality                    & PageRank, HITS, eigenvector,
    \emph{etc} \\
    Link prediction (node similarity)   & SimRank (similarity), PathPredict,
    PathPredict\_when \\
    Clustering graphs (hetero) & RankClus, RankClass \\
    Classification in graphs (hetero) & RankClass, GNetMine \\
    Topic hierarchy of graph & CATHY (homo), CATHYHIN \\
    Disambiguation in bib network & DISTINCT \\
    Truth finding & TruthFinder, Latent Truth Model, Gaussian Truth Model \\
    Frequent pattern mining & Apriori (BFS), FP-growth (DFS) \\
    Sequential pattern mining & GSP, FreeSpan (projections),
    PrefixSpan (no candidate sequence gen.) \\
    Cluster data streams & CluStream \\
    Classification in data streams & VFDT, CVFDT \\
    Pattern mining in data streams & FP-stream \\
    Outliers in data streams & CIDF, hbmix (hierarchical Bayesian approach) \\
    Outliers in general & many techniques; CBLOF \\
    Scalable decision trees & RainForest, BOAT (bootstrapped) \\
    Spatial mining & GeoFolk, GeoTopic, GeoFriend \\
\end{tabular}

\subsection{Old Questions}

\begin{enumerate}
    \item It is interesting to mine homogeneous graphs (or networks) where all
        the nodes and edges are of one type. Name one algorithm for each of the
        following tasks: (1) mining frequent (sub)graph patterns; (2) clustering
        subgraphs in a large graph; and (3) classifying (sub)graphs, given a
        training set of (sub)graphs

        gSpan, SCAN, LEAP.

    \item Use one or two sentences to distinguish the following pairs of
        concepts or methods: (1) PageRank vs SimRank; and (2) support vector
        machines (SVM) vs neural networks

        PR: centrality; SimRank: similarity.

        In an ANN you have a bunch of hidden layers with sizes $h_1$ through
        $h_n$ depending on the number of features, plus bias parameters, and
        those make up your model. By contrast, an SVM (at least a kernelized
        one) consists of a set of support vectors, selected from the training
        set, with a weight for each. In the worst case, the number of support
        vectors is exactly the number of training samples and in general its
        model size scales linearly.

    \item Name or outline one data mining method that best fits each of the
        following tasks: (1) identify patients that are best to be treated by a
        particular therapy; and (2) group bloggers by their political beliefs
        based on their blog contents

        Statistical hypothesis testing, text clustering.

    \item Name three algorithms that efficiently mine frequent patterns,
        sequential patterns, and frequent subgraph patterns from large datasets
        respectively.

        FP-growth, PrefixSpan, gSpan.

    \item Use one or two sentences to distinguish the following pairs of
        concepts or methods: (1) $k$-means vs Expectation-Maximization (EM); and
        (2) support vector machines (SVM) vs probabilistic graphical models

        $k$-means is an instantiation of EM\@.

        SVM is a particular classifier, and PGM are a class of statistical
        models that can be used for many things, including classification.

    \item Name or outline one data mining method that best fits each of the
        following tasks: (1) find rivers and lakes from Google Earth maps; and
        (2) model construction for fraud detection based on online data streams.

        DBSCAN, SGD (or VFDT).

    \item Name one algorithm that efficiently computes data cubes for each of
        the following cases: (i) dense data sets of a small number of
        dimensions, (ii) computing data cubes of a small number of dimensions
        with a given minimum support threshold (also known as iceberg cubes),
        and (iii) assisting query answering in a high-dimensional cube-space.

        Star cubing, BUC, ranking cube(/Shell Fragment Cubing?).

        Bonus: An iceberg cube is a data cube that stores only those cube cells
        that have an aggregate value (e.g., count) above some minimum support
        threshold. Sampling cubes help in statistical data analysis.

    \item Use one or two sentences to distinguish the following pairs of
        concepts or methods: (1) k-Means vs. Expectation-Maximization (EM), and
        (2) Naive Bayes classifier vs probabilistic graphical model.

        Same, NB is a particular instantiation of a PGM\@.

    \item Name or outline one data mining method that best fits each of the
        following tasks: (1) clustering of high-dimensional micro-array data,
        and (2) find frequent items and their approximate frequency counts for
        online data streams.

        CLIFF (or: p-clustering, $\delta$-pCluster, Maple; this is one of
        Jiawei's book chapters), FP-stream.

\end{enumerate}
